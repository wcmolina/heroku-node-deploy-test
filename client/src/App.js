import React, { Component } from 'react';
import './App.css';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import Login from './components/auth/Login';
import Register from './pages/Register';
import Home from './pages/Homepage';

export default class App extends Component {
    render() {
        let loggedIn = false;

        return (
            <BrowserRouter>
                <div>
                    <Switch>
                        <Route exact path="/">
                            {loggedIn ? <Redirect to="/home" /> : <Redirect to="/login" />}
                        </Route>
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/home" component={Home} />
                    </Switch>
                </div>
            </BrowserRouter>
        );
    }
}
