import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HowToVoteIcon from '@material-ui/icons/HowToVote';
import { blue } from '@material-ui/core/colors';

const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    textAlign: 'center',
    paddingTop: '15px',
    fontSize: 30,
    
  },
  pos: {
    marginBottom: 12,
  },
});

export default function OutlinedCard(props) {
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;

  return (
    <Card className={classes.root} variant="outlined">
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {props.ElectionStatus}
        </Typography>
      <CardContent>
        <List component="nav" className={classes.root} aria-label="contacts">
          <ListItem button>
            <ListItemIcon>
              <HowToVoteIcon/>
            </ListItemIcon>
            <ListItemText primary="Chelsea Otakan" />
          </ListItem>
        </List>      
      </CardContent>
    </Card>
  );
}
