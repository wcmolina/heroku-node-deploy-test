import React, { Component } from 'react'
import ListItem from './ListItem';
import {List} from '@material-ui/core'; 
import axios from 'axios';

export default class ListComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            voters: [],
            users: [],
            arrayToRender: [],
            currentPage: 'voters',
            isLoaded: false
          }
      }

    getValues() {
        let url = 'http://balloted-cms.herokuapp.com/api/voters';
        if(this.props.currentPage === 'voters') {
            url = `http://hackathong.herokuapp.com/api/organizations/${this.props.orgId}/voters`;
        } else {
            url = 'http://hackathong.herokuapp.com/api/users';
        }
        console.log('url hehe: ', url);
        return axios.get(url)
        .then((response) => {
            if(this.state.currentPage === 'voters') {
                const voters = response.data;
                console.log("VOTERS: ",voters);
                return voters;
            } else {
                const users = response.data;
                return users;
            }
        })
        .catch(error => {
          return error;
        })
    }

    async componentDidMount() {
        console.log("holiwis props", this.props);
        if(this.props.currentPage) {
            this.setState({currentPage:this.props.currentPage});
        }
        if(this.state.currentPage === 'voters') {
            let voters = await this.getValues();
            this.setState({voters, arrayToRender:voters,isLoaded:true});
        } else {
            let users = await this.getValues();
            this.setState({users, arrayToRender:users,isLoaded:true});
        }
    }


    render() {
        if(this.state.isLoaded){
            return (
                <div>
                <List>
                   {this.state.arrayToRender.map(value => {
                       let props = {value:value, currentPage: this.state.currentPage}
                        return <ListItem>{props}</ListItem>
                        })
                    }
                </List> 
                </div>
            )
        } else {
            return(
                <div/>
            )
        }
    }
}
