import React, { Component, useState} from 'react';
import {
        ListItem, 
        ListItemIcon, 
        ListItemText, 
        ListItemAvatar,
        IconButton,
        Avatar,
        ListItemSecondaryAction} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Voter from '../models/Voter';
import User from '../models/User';

export default class ListItemComp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            voter: new Voter([]),
            user: new User([]),
            currentPage: "voters",
          }
      }

    stateString(state) {
        console.log("user state", state);
        let stateString = "";
        switch(state){
            case "Approved":
                stateString = "Approved";
                break;
            case "pending_invite":
                stateString = "Pending Invite";
                break;
            case "pending_approval":
                stateString = "Pending Approval";
                break;
            case "rejected":
                stateString = "Rejected";
                break;
            default:
                stateString = "Status Not Found";
        }
        return stateString;
    }

    componentDidMount() {
        //console.log('voters aqui de que u', this.state.voter);
        console.log("props : ",this.props.children);
        if(this.props.children.currentPage) {
            this.setState({currentPage:this.props.children.currentPage});
        }
        if(this.state.currentPage === 'voter') {
            let voter = new Voter([]);
            if(this.props.children.value) {
                voter = new Voter (this.props.children.value);
            } else {
                voter = new Voter({"id":6,"first_name":"Mark","second_name":"Padilla","registration_id":"78c003e4-5759-4a3a-bbe5-b375794a77d2","email":"mark@email.com","blockchain_address":"iagfudan289y","created_at":"2020-02-23T07:53:19.357Z","updated_at":"2020-02-23T07:53:19.357Z","form_first_name":"","form_second_name":"","form_registration_id":"","form_email":"","voter_status":"Approved","photo_selfie":null,"photo_id":null});
            }
            this.setState({voter});
        } else {
            let user = new User([]);
            if(this.props.children.value) {
                user = new User(this.props.children.value);
            } else {
                user = new User({"id":6,"first_name":"Mark","second_name":"Padilla","registration_id":"78c003e4-5759-4a3a-bbe5-b375794a77d2","email":"mark@email.com","blockchain_address":"iagfudan289y","created_at":"2020-02-23T07:53:19.357Z","updated_at":"2020-02-23T07:53:19.357Z","form_first_name":"","form_second_name":"","form_registration_id":"","form_email":"","voter_status":"Approved","photo_selfie":null,"photo_id":null});
            }
            this.setState({user});
        }
    }

    valueToRender() {
        let value = {firstString: '', secondString: ''};
        if(this.state.currentPage === 'voter') {
            value.firstString = this.state.voter.firstName + ' ' +this.state.voter.lastName;
            value.secondString = this.state.string(this.state.voter.Status);
        } else {
            value.firstString = this.state.user.name;
            value.secondString = this.state.user.profile;
        }
        return value;
    }

    buttonsShowing() {
        if(this.state.currentPage ==='users') {
            return <ListItemSecondaryAction/>
        } else {
            return <ListItemSecondaryAction>
            <IconButton edge="end" aria-label="edit">
                <EditIcon />
            </IconButton>
          <IconButton edge="end" aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
        }
    }

    render() {
        return (
            <ListItem>
            <ListItemAvatar>
              <Avatar/>
            </ListItemAvatar>
            <ListItemText
              primary={this.valueToRender().firstString}
              secondary={this.valueToRender().secondString}
            />
            {this.buttonsShowing()}
          </ListItem>
        )
    }
}
