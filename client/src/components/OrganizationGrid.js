import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import {
  GridList,
  GridListTile,
  GridListTileBar,
  ButtonBase
} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    marginTop: "20px",
    width: "100%",
    height: "100%"
  },
  image: {
    position: "relative",
    height: 200,
    margin: "1em",
    [theme.breakpoints.down("xs")]: {
      width: "100% !important", // Overrides inline-style
      height: 100
    },
    "&:hover, &$focusVisible": {
      zIndex: 1,
      "& $imageBackdrop": {
        opacity: 0.2
      },
      "& $imageMarked": {
        opacity: 0
      },
      "& $imageTitle": {
        border: "4px solid currentColor"
      }
    }
  },
  focusVisible: {},
  imageSrc: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: "cover",
    backgroundPosition: "center 40%"
  },
  imageBackdrop: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0,
    transition: theme.transitions.create("opacity")
  },
  imageTitle: {
    position: "relative",
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${theme.spacing(1) +
      6}px`
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: "absolute",
    bottom: -2,
    left: "calc(50% - 9px)",
    transition: theme.transitions.create("opacity")
  }
  // OrgsImg: {
  //   width: '50%',
  //   height: "100%"
  // },
  // OrgsTile: {
  //   width: '100%',
  //   height: "25%"
  // }
}));

const OrganizationGrid = props => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <p>Recent</p>
      <GridList cellHeight={180} className={classes.gridList}>
        {props.organizationData.map(tile => (
          <ButtonBase
            focusRipple
            key={props.image}
            className={classes.image}
            focusVisibleClassName={classes.focusVisible}
            cols={0.62}
            rows={2}
            onClick = {() => props.handleOrganizationClick(tile,false,tile.ipfs_hash, tile.id)}>
            <span
              className={classes.imageSrc}
              style={{
                backgroundImage: "url(" + props.imgUrl + ")",
                backgroundSize: "contain",
                backgroundRepeat: "no-repeat"
              }}
            />
            <span className={classes.imageBackdrop} />
            <span className={classes.imageButton}>
              <GridListTile component="span" color="inherit">
                <GridListTileBar className={classes.imageTitle} title={tile.ipfs_hash} />
              </GridListTile>
            </span>
          </ButtonBase>
        ))}
      </GridList>
    </div>
  );
};

export default OrganizationGrid;
