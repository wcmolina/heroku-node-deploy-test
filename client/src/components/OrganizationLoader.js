import React, { Component } from "react";
import OrganizationGrid from "./OrganizationGrid";
import User from "../models/User";
import AddIcon from "@material-ui/icons/Add";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";
import SimpleModal from "./SimpleModal";

class OrganizationLoader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      organizations: [],
      user: new User([]),
      modalOpen: false,
      modalStyle: this.getModalStyle()
    };
  }

  rand() {
    return Math.round(Math.random() * 20) - 10;
  }
  getModalStyle() {
    const top = 50;
    const left = 55;
    return {
      top: `${top}%`,
      left: `${left}%`,
      transform: `translate(-${top}%, -${left}%)`
    };
  }

  handleOpen = () => {
    this.setState({ modalOpen: true });
  };

  handleClose = () => {
    this.setState({ modalOpen: false });
  };

  addOrgButton = user => {
    return this.state.user.profile === "Admin" ? (
      <div>
        <Button
          variant="contained"
          color="secondary"
          //className={classes.button}
          startIcon={<AddIcon />}
          variant="contained"
          color="primary"
          onClick={this.handleOpen}
        >
          Add
        </Button>
        <SimpleModal
          open={this.state.modalOpen}
          handleClose={this.handleClose}
          modalStyle={this.state.modalStyle}
        />
      </div>
    ) : (
      <br />
    );
  };

  componentDidMount() {
    if (this.props.user) {
      this.setState({ user: this.props.user });
    }
    fetch("https://hackathong.herokuapp.com/api/organizations")
      .then(res => res.json())
      .then(data => {
        this.setState({ organizations: data });
      })
      .catch(console.log);
  }

  render() {
    return (
      <div>
        <Typography component="div" color="primary">
          <Box fontSize={26} m={1} fontWeight="fontWeightBold" align="center">
            Organizations
            {this.addOrgButton()}
          </Box>
        </Typography>
        <OrganizationGrid
          organizationData={this.state.organizations}
          imgUrl={"/img/Logo-BL.png"}
          handleOrganizationClick={this.props.handleOrganizationClick}
        />
      </div>
    );
  }
}

export default OrganizationLoader;
