import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 360
    }
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    position: "absolute",
    width: 450,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    justifyContent: "center",
    alignItems: "center"
  },
  h2: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex"
  }
}));

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color="default" {...props} />);

const SimpleModal = props => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    require_photo: false,
    ipfs_hash: "",
    blockchain_address: ""
  });

  const handleChange = name => event => {
    setState({ ...state, [name]: event.target.checked });
  };

  function handleTextChange(event) {
    if (event.target.id === "ipfs") {
      setState({
        ipfs_hash: event.target.value,
        blockchain_address: state.blockchain_address,
        require_photo: state.require_photo
      });
    } else {
      setState({
        blockchain_address: event.target.value,
        ipfs_hash: state.ipfs_hash,
        require_photo: state.require_photo
      });
    }
  }
  const handleSubmit = () => {
    console.log(state);
    axios
      .post(`https://hackathong.herokuapp.com/api/organizations`, state)
      .then(res => {
        console.log(res);
        console.log(res.data);
        window.location.reload();
      })
      .catch(function(error) {
        console.log(error);
      });
    props.handleClose();
  };

  return (
    <div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.open}
        onClose={props.handleClose}
      >
        <div style={props.modalStyle} className={classes.paper}>
          <h2 className={classes.h2}>Create Organization</h2>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              id="ipfs"
              label="ipfs hash"
              variant="filled"
              onChange={handleTextChange}
            />
            <TextField
              id="blockchain"
              label="blockchain address"
              variant="filled"
              onChange={handleTextChange}
            />
            <FormControlLabel
              control={
                <GreenCheckbox
                  checked={state.checkedG}
                  onChange={handleChange("require_photo")}
                  value="required_photo"
                />
              }
              label="Require photo"
            />
          </form>

          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit.bind(this)}
          >
            Create
          </Button>
        </div>
      </Modal>
    </div>
  );
};

export default SimpleModal;
