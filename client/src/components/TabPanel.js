import React, {useState} from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import PersonPinIcon from "@material-ui/icons/PersonPin";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import ApartmentIcon from "@material-ui/icons/Apartment";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import AddIcon from "@material-ui/icons/Add";
import List from "./List";
import ElectionsList from "./ElectionsList"
import VotersModal from "./VotersModal"

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-force-tabpanel-${index}`}
      aria-labelledby={`scrollable-force-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `scrollable-force-tab-${index}`,
    "aria-controls": `scrollable-force-tabpanel-${index}`
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    width: "100%",
    backgroundColor: theme.palette.background.paper
  }
}));

const ScrollableTabsButtonForce = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const propsForVoters = ({currentPage: "voters", orgId: props.organizationId});

  function addVotersButton(user) {
    console.log("user here", user);
    return props.user.profile === "Admin" ? (
      <div>
        <Button
          variant="contained"
          color="secondary"
          //className={classes.button}
          startIcon={<AddIcon />}
          variant="contained"
          color="primary"
          onClick={handleOpen}
        >
          Add
        </Button>
        <VotersModal
          open={modalOpen}
          handleClose={handleClose}
          modalStyle={getModalStyle()}
          organizationID={propsForVoters.orgId}
        />
      </div>
    ) : (
      <br />
    );
  };

  function handleOpen() {
    setModalOpen(true);
  };

  function handleClose() {
    setModalOpen(false);
  };

  function getModalStyle() {
      const top = 50;
      const left = 55;
      return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`
      };
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          variant="scrollable"
          scrollButtons="on"
          indicatorColor="primary"
          textColor="primary"
          aria-label="scrollable force tabs example"
        >
          <Tab label="Elections" icon={<AssignmentTurnedInIcon />} {...a11yProps(0)}/>
          <Tab label="Voters" icon={<PersonPinIcon />} {...a11yProps(1)} />
          <Tab label="Authorities" icon={<ApartmentIcon />} {...a11yProps(2)} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <ElectionsList/>
      </TabPanel>
      <TabPanel value={value} index={1}>
      <Typography component="div" color="primary">
          <Box fontSize={26} m={1} fontWeight="fontWeightBold" align="center">
            {props.organizationName + " Voters"}
            {addVotersButton(props.user)}
          </Box>
        </Typography>
        <List {...propsForVoters}/>
      </TabPanel>
      <TabPanel value={value} index={2}>
        Insertar tabla de authorities
      </TabPanel>
    </div>
  );
}

export default ScrollableTabsButtonForce;
