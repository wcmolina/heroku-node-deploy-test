import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { withStyles } from "@material-ui/core/styles";
import { green } from "@material-ui/core/colors";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TextField from "@material-ui/core/TextField";
import axios from "axios";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 360
    }
  },
  modal: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  paper: {
    position: "absolute",
    width: 450,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    justifyContent: "center",
    alignItems: "center"
  },
  h2: {
    justifyContent: "center",
    alignItems: "center",
    display: "flex"
  }
}));

const GreenCheckbox = withStyles({
  root: {
    color: green[400],
    "&$checked": {
      color: green[600]
    }
  },
  checked: {}
})(props => <Checkbox color="default" {...props} />);

const VotersModal = (props) => {
  const classes = useStyles();
  const [state, setState] = React.useState({
    first_name: "",
    second_name: "",
    registration_id: "",
    email: "",
    organization_id: props.organizationId
  });

  function handleTextChange(event) {
    // if (event.target.id === "first_name") {
      switch(event.target.id) {
        case "first_name":
            setState({
                first_name: event.target.value,
                second_name: state.second_name,
                registration_id: state.registration_id,
                email: state.email,
                organization_id: props.organizationId
              });
            break;
        case "second_name":
            setState({
                first_name: state.first_name,
                second_name: event.target.value,
                registration_id: state.registration_id,
                email: state.email,
                organization_id: props.organizationId
               });
            break;
        case "email":
            setState({
                first_name: state.first_name,
                second_name: state.second_name,
                registration_id: state.registration_id,                    
                email: event.target.value,
                organization_id: props.organizationId
            });
            break;
        case "registration_id":
            setState({
                first_name: state.first_name,
                second_name: state.second_name,
                registration_id: event.target.value,
                email: state.email,
                organization_id: props.organizationId
              });
            break;
      }
    // } else {
    //   setState({
    //     second_name: event.target.value,
    //     first_name: state.first_name,
    //     registration_id: state.resgistration_id,
    //     email:state.email
    //   });
    // }
  }
  const handleSubmit = () => {
    console.log(state);
    axios
      .post(`https://hackathong.herokuapp.com/api/voters`, state)
      .then(res => {
        console.log(res);
        console.log(res.data);
        window.location.reload();
      })
      .catch(function(error) {
        console.log(error);
        console.log(state);
      });
    props.handleClose();
  };

  return (
    <div>
      <Modal
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        open={props.open}
        onClose={props.handleClose}
      >
        <div style={props.modalStyle} className={classes.paper}>
          <h2 className={classes.h2}>Create Organization</h2>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              id="first_name"
              label="First Name"
              variant="filled"
              onChange={handleTextChange}
            />
            <TextField
              id="second_name"
              label="Last Name"
              variant="filled"
              onChange={handleTextChange}
            />
            <TextField
              id="registration_id"
              label="Registration ID"
              variant="filled"
              onChange={handleTextChange}
            />
            <TextField
              id="email"
              label="Email"
              variant="filled"
              onChange={handleTextChange}
            />
          </form>

          <Button
            variant="contained"
            color="primary"
            onClick={handleSubmit.bind(this)}
          >
            Create
          </Button>
        </div>
      </Modal>
    </div>
  );
};

export default VotersModal;
