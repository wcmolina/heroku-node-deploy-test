import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
//import { Link as Anchor } from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import React, { useState } from 'react';
import axios from 'axios';
import { Link, Redirect, useHistory } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh'
    },
    image: {
        //backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundImage: "url('img/Ilustration_1.png')",
        backgroundRepeat: 'no-repeat',
        backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#48BFB7'
    },
    logo: {
        width: '200px',
        height: '200px'
    },
    typo: {
        color: '#747c7c'
    }
}));

const Login = props => {
    const classes = useStyles();
    let history = useHistory();

    const [formData, setFormData] = useState({
        userName: '',
        password: ''
    });

    const { userName, password } = formData;

    const onChange = e => setFormData({ ...formData, [e.target.name]: e.target.value });

    const onSubmit = async e => {
        e.preventDefault();
        login(userName, password);
    };

    const login = async (userName, password) => {
        const config = {
            headers: {
                'Content-Type': 'application/json'
            }
        };

        const body = JSON.stringify({ userName, password });

        try {
            const res = await axios.post('/api/auth', body, config);
            localStorage.setItem('token', res.data.token);
            history.push('/home');
        } catch (error) {
            alert('Invalid Credentials.');
        }
    };

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <img src="/img/Logo-BL.png" className={classes.logo}></img>
                    <Typography className={classes.typo} component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={e => onSubmit(e)}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Username"
                            name="userName"
                            autoComplete="username"
                            autoFocus
                            onChange={e => onChange(e)}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={e => onChange(e)}
                        />
                        <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>

                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link to="/register">Don't have an account? Sign up</Link>
                            </Grid>
                        </Grid>

                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
};

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'© HELLO ICONIC '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

export default Login;
