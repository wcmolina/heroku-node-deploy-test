import React, { Fragment } from 'react';
import logo from '../../img/app_logo.png';

export default () => (
    <Fragment>
        <img src={logo} style={{ width: '75px', margin: 'auto', display: 'block' }} alt="Logo" />
    </Fragment>
);
