class Organization {
  constructor(organization) {
    let self = this;
    this.ipfs_hash = organization.ipfs_hash?organization.ipfs_hash:"ipfs_hash";
    this.blockchain_address = organization.blockchain_address?organization.blockchain_address:"blockchain_address";
    this.require_photo = organization.require_photo?organization.require_photo:true;
  }
}

export default Organization;
