class User {
    constructor(user) {
        let self = this;
        this.id = user.id? user.id : 0;
        this.username = user.user_name? user.user_name:"username";
        this.name = user.complete_name? user.complete_name:"Complete Name";
        this.email = user.email? user.email:"hello@email.com";
        this.profile = user.profile? user.profile : "Admin";
    }
}

export default User;
