class Voter {
    constructor(voter) {
        let self = this;
        this.id = voter.registration_id? voter.registration_id : "";
        this.firstName = voter.first_name? voter.first_name:"FirstName";
        this.lastName = voter.second_name? voter.second_name:"Last Name";
        this.email = voter.email? voter.email:"hello@email.com";
        this.status = voter.voter_status? voter.voter_status : "pending_invite";
        this.createdAt = voter.created_at? voter.created_at : "timestamp1";
        this.updatedAt = voter.updated_at? voter.updated_at : "timestamp1";
    }
}

export default Voter;

