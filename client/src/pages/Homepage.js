import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import List from "@material-ui/core/List";
import LocalList from "../components/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Box from "@material-ui/core/Box";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import User from "../models/User";
import OrganizationLoader from "../components/OrganizationLoader";
import TabPanel from "../components/TabPanel";
import useForceUpdate from "use-force-update";

const drawerWidth = 240;
const useStyles = makeStyles(theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth
    }
  },
  menuButton: {
    marginRight: theme.spacing(2),
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3)
  },
  sideBarIcons: {
    width: "25px",
    height: "25px"
  },
  logo: {
    width: "200px",
    height: "200px",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginLeft: "auto",
    marginRight: "auto"
  },
  userIcon: {
    marginLeft: "auto"
  },
  userIconName: {
    fontSize: "17px",
    marginLeft: "87%"
  },
  navBar: {
    backgroundColor: "#4C6085"
  }
}));

var loadOrganizations = true;
var loadUsers = false;
var orgName = "";
var orgId = "";
function ResponsiveDrawer(props) {
  const { container } = props;
  const classes = useStyles();
  const theme = useTheme();
  let user = new User({complete_name:"David Cruz"});
  const [mobileOpen, setMobileOpen] = React.useState(false);
  const linkList =
    user.profile === "Admin" ? ["Organizations", "Users"] : ["Organizations"];

  console.log("USER:", user);

  const completeName =  (
  <Typography variant="h6" className={classes.userIconName}>
    {user.name}
  </Typography>
  )
  
  const handleDrawerToggle = () => {
    setMobileOpen(!mobileOpen);
  };
  const forceUpdate = useForceUpdate();
  const handleOrganizationClick = (item,loadOrgs,name,id) => {
    orgId = id;
    orgName = name;
    loadOrganizations = loadOrgs;
    forceUpdate();
    console.log("organizationName:" + orgName);
  };

  function renderContent(loadOrganizations, loadUsers) {
    if (loadOrganizations) {
      return (
        <OrganizationLoader handleOrganizationClick={handleOrganizationClick} />
      );
    } else if(loadUsers) {
      const propsForUsers = ({currentPage: "users"});
      console.log("holis golis", propsForUsers)
      return(
        <div>
        <Typography component="div" color="primary">
          <Box fontSize={26} m={1} fontWeight="fontWeightBold" align="center">
            Users
          </Box>
        </Typography>
        <LocalList {...propsForUsers}/>
        </div>
      );
    } else {
    return <TabPanel organizationName={orgName} organizationId={orgId} user = {user}/>;
    }
  }

  function sidebarClicked(item)
  {
      switch(item)
      {
        case 0: //Organizations
          loadOrganizations = true;
          loadUsers = false;
        break;

        case 1: //Users
          loadUsers = true;
          loadOrganizations = false;
        break;
      }
      forceUpdate();
  }

  const drawer = (
    <div>
      <div className={classes.toolbar}>
        <img src="img/Logo-BL.png" className={classes.logo}></img>
      </div>
      <Divider />
      <List>
        {linkList.map((text, index) => (
          <ListItem button key={text}
           onClick={() => sidebarClicked(index)}>
            <ListItemIcon>
              {index % 2 === 0 ? (
                <img
                  className={classes.sideBarIcons}
                  src="img/company.png"
                ></img>
              ) : (
                <img className={classes.sideBarIcons} src="img/user.png"></img>
              )}
            </ListItemIcon>
            <ListItemText primary={text} />
          </ListItem>
        ))}
      </List>
      <Divider />
    </div>
  );

  
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar className={classes.navBar}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <MenuIcon />
          </IconButton>   
         {completeName}
         <AccountCircle
            className= {classes.userIcon}
         >
         </AccountCircle>
      
        </Toolbar>
      </AppBar>
      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            container={container}
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen}
            onClose={handleDrawerToggle}
            classes={{
              paper: classes.drawerPaper
            }}
            ModalProps={{
              keepMounted: true // Better open performance on mobile.
            }}
          >
            {drawer}
          </Drawer>
        </Hidden>
        <Hidden xsDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper
            }}
            variant="permanent"
            open
          >
            {drawer}
          </Drawer>
        </Hidden>
      </nav>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {renderContent(loadOrganizations, loadUsers)}
      </main>
    </div>
  );
}

ResponsiveDrawer.propTypes = {
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  container: PropTypes.any
};

export default ResponsiveDrawer;
