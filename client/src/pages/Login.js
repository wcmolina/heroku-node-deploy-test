import React, { Redirect, Route } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { validate } from '@babel/types';
import { useHistory } from 'react-router-dom';
import Base64 from 'base-64';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'© HELLO ICONIC '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles(theme => ({
    root: {
        height: '100vh'
    },
    image: {
        //backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundImage: "url('img/Ilustration_1.png')",
        backgroundRepeat: 'no-repeat',
        backgroundColor: theme.palette.type === 'dark' ? theme.palette.grey[900] : theme.palette.grey[50],
        backgroundSize: 'cover',
        backgroundPosition: 'center'
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
        backgroundColor: '#48BFB7'
    },
    logo: {
        width: '200px',
        height: '200px'
    },
    typo: {
        color: '#747c7c'
    }
}));

export default function SignInSide() {
    const classes = useStyles();
    var password = '';
    var username = '';
    let history = useHistory();

    //this.handleChange = this.handleChange.bind(this);
    //this.handleSubmit = this.handleSubmit.bind(this);

    function handleChange(event) {
        if (event.target.id === 'email') {
            username = event.target.value;
            console.log(username);
        } else {
            password = event.target.value;
        }
    }

    function validateEmail(string) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(string).toLowerCase());
    }

    function handleSubmit(event) {
        event.preventDefault();
        var emailValidation = true;
        if (username !== '' && password !== '') {
            if (username.includes('@') || username.includes('.com')) {
                emailValidation = validateEmail(username);
            }
            if (!emailValidation) {
                console.log('Not a valid email');
            } else {
                //TODO: POST TO API WITH PASSWORD AND USERNAME
                console.log('submit:', password, username);
                //history.push("/home");
                signIn(username, password);
            }
        } else {
            console.log('password AND username are required fields');
        }
    }

    function signIn(email, password) {
        //https://balloted-cms.herokuapp.com/api/auth
        fetch('https://balloted-cms.herokuapp.com/api/auth', {
            method: 'GET',
            headers: new Headers({
                Authorization: 'Basic ' + Base64.encode(email + ':' + password)
            })
        })
            .then(res => res.json())
            .then(data => {
                console.log('Auth Success' + data);
                history.push('/home');
            })
            .catch(err => {
                console.log('Error Authenticating: ', err);
                alert('Invalid User / Password.');
            });
    }

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <img src="/img/Logo-BL.png" className={classes.logo}></img>
                    <Typography className={classes.typo} component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email Address"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            onChange={handleChange.bind(this)}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={handleChange.bind(this)}
                        />
                        <FormControlLabel control={<Checkbox value="remember" color="primary" />} label="Remember me" />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="secondary"
                            className={classes.submit}
                            onClick={handleSubmit}
                        >
                            Sign In
                        </Button>

                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link href="/register">Don't have an account? Sign up</Link>
                            </Grid>
                        </Grid>

                        <Box mt={5}>
                            <Copyright />
                        </Box>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}
