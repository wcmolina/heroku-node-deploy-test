const pgp = require('pg-promise')(),
config = process.env.DATABASE_URL || 'postgres://api_user@localhost:5432/hackathong',
db= pgp(config);
const returnError = (error = true, data = []) => {
    return {
        error: error ? error.detail || error : false,
        data
    };
};
module.exports = {
    db: db,
    list: async (table, where = {}) => {
        try {
            where = typeof(where) !== 'object' ? {id: where} : where;
            const keyValues = Object.keys(where);
            let query = `SELECT * FROM ${table} ${keyValues.length > 0 ? ' WHERE ' : ''}`;
            keyValues.forEach((_key, index) => {
                query += _key + ` = '${where[_key]}' ${index < keyValues.length - 1 ? ' AND ' : ';'}`;
            });
            console.log("@query", query);
            const data = await db.query(query);
            return {
                data,
                error: false
            };
        } catch (e) {
            console.log(e)
            return returnError(e);
        }
    },
    delete: async (table, id) => {
        try {
            const query = `DELETE FROM ${table} WHERE id = ${id}`;
            await db.none(query);
            return {
                error: false
            };
        } catch (e) {
            return returnError(e);
        }
    },
    insert: async (table, values) => {
        try {
            const keyValues = Object.keys(values);
            let query = `INSERT INTO ${table} (${keyValues.toString()}) VALUES (`;
            const valuesToSave = keyValues.map((_key, index) => {
                query += `$${index + 1}${index < keyValues.length - 1 ? ',' : ') returning *'}`;
                return values[_key];
            });
            console.log('@query', query);
            console.log('@valuesToSave', valuesToSave);
            const data = await db.one(query, valuesToSave);
            return {
                error: false,
                data
            }
        } catch(e) {
            console.log(e)
            return returnError(e);
        }
    },
    listJoin: async (table,ref,id)=> {
        try {
            const query = `SELECT * FROM ${table} ${id ? `WHERE ${ref} = ${id};` : ';'}`;
            // console.log("@query", query);
            const data = await db.query(query);
            return {
                data,
                error: false
            };
        } catch (e) {
            return returnError(e);
        }        
    },
    update: async (table, id, values) => {
        try{
            let query = `UPDATE ${table} SET `;
            const keyValues = Object.keys(values);
            let valuesToSave = [];
            keyValues.forEach((_key, index) => {
                if (_key !== 'id' && values[_key] !== undefined) {
                    query += `${_key} = $1 ${index < keyValues.length - 1 ? ',' : ''}`;
                    valuesToSave.push(values[_key]);
                }
            });
            query += ` WHERE id = ${id} RETURNING *;`;
            const data = await db.oneOrNone(query, valuesToSave);
            return {
                data,
                error: false
            };
        } catch(e) {
            console.log(e)
            return returnError(e);
        }
    },
    list_username: async (username) => {
        try {
            const query = `SELECT * FROM users INNER JOIN roles ON users.role_id = roles.id ${username ? `WHERE user_name = '${username}';` : ';'}`;
            console.log("@query", query);
            const data = await db.query(query);
            return {
                data,
                error: false
            };
        } catch (e) {
            return {
                data: {},
                error: true
            };
        }
    }

};