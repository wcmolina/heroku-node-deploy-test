INSERT INTO roles(name)
VALUES ('Admin'), ('Authority');

INSERT INTO voters_status(name)
VALUES('Pending_Invite'),('Pending_Approval'),('Approved'),('Rejected'),('Archived');

INSERT INTO election_status(name)
VALUES ('Upcoming'),('Open'),('Closed');