// Imports
const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
    // Get token from header
    const bearerHeader = req.headers.authorization;

    // Check if there's an authorization header
    // Authorization: Bearer <token>
    if (bearerHeader) {
        const bearer = bearerHeader.split(' ');
        const token = bearer[1];
        if (token) {
            try {
                const decoded = jwt.verify(token, process.env.JWT_SECRET);
                // Get user ID from decoded payload
                req.user = decoded.user;
                next();
            } catch (error) {
                return res.status(401).json({ msg: 'Token is not valid' });
            }
        } else {
            return res.status(401).json({ msg: 'Token is not valid' });
        }
    } else {
        return res.status(401).json({ msg: 'No token, authorization denied' });
    }
};
