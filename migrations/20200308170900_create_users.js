exports.up = function(knex) {
    return knex.schema.createTable('users', table => {
        table.increments('id').primary();
        table
            .string('userName', 30)
            .unique()
            .notNullable();
        table.string('firstName', 30).notNullable();
        table.string('lastName', 30).notNullable();
        table
            .string('blockchainAddress')
            // .unique() // Uncommenting for development purposes, but this should be unique
            .notNullable();
        table
            .string('email')
            .unique()
            .notNullable();
        table.enu('role', ['Admin', 'Authority']);
        table.string('password').notNullable();
        table.timestamp('createdAt').defaultTo(knex.fn.now());
    });
};

exports.down = function(knex) {
    return knex.schema.dropTable('users');
};
