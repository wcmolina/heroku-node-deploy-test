const { Model } = require('objection');

class User extends Model {
    static get tableName() {
        return 'users';
    }

    static get jsonSchema() {
        return {
            type: 'object',
            required: ['userName', 'firstName', 'lastName', 'blockchainAddress', 'email', 'role', 'password'],

            properties: {
                id: { type: 'integer' },
                userName: { type: 'string', pattern: '^[a-zA-Z0-9-_]+$' },
                firstName: { type: 'string', minLength: 1, maxLength: 30, pattern: '^[A-Za-zÀ-ÿ ]+$' },
                lastName: { type: 'string', minLength: 1, maxLength: 30, pattern: '^[A-Za-zÀ-ÿ ]+$' },
                blockchainAddress: { type: 'string', minLength: 40, maxLength: 255 },
                email: {
                    type: 'string',
                    minLength: 3,
                    maxLength: 255,
                    pattern: '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'
                },
                role: { type: 'string', enum: ['Admin', 'Authority'], default: 'Authority' },
                createdAt: { type: 'string', default: new Date().toISOString() }
            }
        };
    }
}

module.exports = User;
