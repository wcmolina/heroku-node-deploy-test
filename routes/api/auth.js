const express = require('express');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');
const auth = require('../../middleware/auth');

const router = express.Router();

// @route   GET api/auth
// @desc    Get authenticated user from token
// @access  Private

router.get('/', auth, async (req, res) => {
    try {
        const user = await User.query().findOne('id', req.user.id);
        res.json(user.$omit('password'));
    } catch (error) {
        res.status(500).send('Server error');
    }
});

// @route   POST api/auth
// @desc    Authenticate user and get token (login)
// @access  Public

router.post('/', async (req, res) => {
    const { userName, password } = req.body;

    // Check if username is present
    if (userName) {
        try {
            const user = await User.query().findOne('userName', userName);
            if (!user) {
                return res.status(400).json({ errors: [{ msg: 'Invalid credentials' }] });
            }

            // Check if password matches
            const passwordMatches = await bcrypt.compare(password, user.password);
            if (!passwordMatches) {
                return res.status(400).json({ errors: [{ msg: 'Invalid credentials' }] });
            }

            // Setup JWT payload
            const payload = {
                user: {
                    id: user.id
                }
            };

            // Return JWT
            jwt.sign(
                payload,
                process.env.JWT_SECRET,
                {
                    expiresIn: '180d'
                },
                (err, token) => {
                    if (err) throw err;
                    return res.json({ token });
                }
            );
        } catch (error) {
            return res.status(500).send('Server error');
        }
    } else {
        return res.status(400).json({ errors: [{ msg: 'Invalid credentials' }] });
    }
});

module.exports = router;
