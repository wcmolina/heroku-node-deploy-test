const express = require('express');
const { ValidationError, UniqueViolationError } = require('objection');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../../models/User');

const router = express.Router();

// @route   POST api/users
// @desc    Register new user (sign up)
// @access  Private

router.post('/', async (req, res) => {
    try {
        const { userName, firstName, lastName, email, password } = req.body;

        // Check if user already exists
        let user = await User.query()
            .where('userName', userName)
            .orWhere('email', email)
            .first();

        if (user) {
            return res.status(403).send('User already exists');
        }

        // Placeholder while web3 logic is integrated here
        const blockchainAddress = '0x0918b72e5e5fd3b2EDB62F896A090E2A09776398';

        // Create user object
        user = {
            userName,
            firstName,
            lastName,
            blockchainAddress,
            email,
            password
        };

        // Encrypt password
        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);

        // Insert user to database
        user = await User.query()
            .insert(user)
            .returning('*');

        // Setup JWT payload
        const payload = {
            user: {
                id: user.id
            }
        };

        // Return JWT
        jwt.sign(
            payload,
            process.env.JWT_SECRET,
            {
                expiresIn: '180d'
            },
            (err, token) => {
                if (err) throw err;
                return res.json({ token });
            }
        );
    } catch (error) {
        if (error instanceof ValidationError || error instanceof UniqueViolationError) {
            return res.status(400).json({ errors: error.data });
        }
        return res.status(500).send('Server error');
    }
});

module.exports = router;
