const express = require('express');
const router = express.Router();
const config = require('../db/config');

router.post('/', async (request, response)  =>  {
	var username = request.body.username;
    var password = request.body.password;
    console.log("Username: " + username + " Password: " + password);
	if (username && password) {
        const data = await config.list_username(username);
        if(data.data[0]){
            if(data.data[0].password == password){
                request.session.loggedin = true;
                request.session.username = username;
                request.session.role = data.data[0].name;

                response.redirect('/home');
            }else{
                response.status(401)
            }
        }else{
            response.status(403)
        }
        response.end();
	} else {
		response.send('Please enter username and password');
		response.end();
	}
});

module.exports = router