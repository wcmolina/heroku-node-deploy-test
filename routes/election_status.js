const express = require('express');
const router = express.Router();
const config = require('../db/config');
const utils = require('../utils');

router.get('/', async (req, res) => {
    const data = await config.list('election_status');
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.get('/:id', async (req, res) => {
    const data = await config.list('election_status', req.params.id);
    utils.setStatus(res, data.error);
    res.json(data.data);
});

router.post('/', async (req, res) => {
    const data = await config.insert('election_status', {
        name: req.body.name
    });
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.delete("/:id", async (req, res) => {
    const data = await config.delete('election_status', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});
router.put('/:id', async (req, res) => {
    const data = await config.update('election_status', req.params.id, {
        name: req.body.name
    });
    utils.setStatus(res, data.error);
    res.json(data.data);
});
module.exports = router