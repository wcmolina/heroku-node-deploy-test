const express = require('express');
const router = express.Router();
const config = require('../db/config');
const utils = require('../utils');

const getBodyData = req => {
    return {
        status: req.body.status,
        organization_id: req.body.organization_id,
        ipfs_hash: req.body.ipfs_hash,
        show_results: req.body.show_results,
        start_date: req.body.start_date,
        close_date: req.body.close_date,
        blockchain_address: req.body.blockchain_address
    };
}

router.get('/', async (req, res) => {
    // const data = await config.list('elections');
    // utils.setStatus(res, data.error);
    // res.json(data.data);
    const query = `SELECT elections.*, election_status.name AS status FROM elections INNER JOIN election_status ON elections.status = election_status.id;`
    const data = await config.db.query(query);
    res.json(data);
});
router.get('/:id', async (req, res) => {
    // const data = await config.list('elections', req.params.id);
    // utils.setStatus(res, data.error);
    // res.json(data.data);
    const query = `SELECT elections.*, election_status.name AS status FROM elections INNER JOIN election_status ON elections.status = election_status.id WHERE elections.id = ${req.params.id};`
    const data = await config.db.query(query);
    res.json(data);
});

router.post('/', async (req, res) => {
    console.log(req.body)
    const data = await config.insert('elections', getBodyData(req));
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.delete("/:id", async (req, res) => {
    const data = await config.delete('elections', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});
router.put('/:id', async (req, res) => {
    const data = await config.update('elections', req.params.id, getBodyData(req));
    utils.setStatus(res, data.error);
    res.json(data.data);
});
module.exports = router