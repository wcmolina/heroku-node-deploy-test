const express = require('express');
const router = express.Router();
const config = require('../db/config');
const utils = require('../utils');

router.get('/', async (req, res) => {
    const data = await config.list('organization_users');
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.get('/:id', async (req, res) => {
    const data = await config.list('organization_users', req.params.id);
    utils.setStatus(res, data.error);
    res.json(data.data);
});

router.post('/', async (req, res) => {
    const data = await config.insert('organization_users', {
        organization_id: req.body.organization_id,
        user_id: req.body.user_id
    });
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.delete("/:id", async (req, res) => {
    const data = await config.delete('organization_users', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});
// router.put('/:id', async (req, res) => {
//     const data = await config.update('organization_users', req.params.id, {
//         organization_id: req.body.organization_id,
//         user_id: req.body.user_id
//     });
//     utils.setStatus(res, data.error);
//     res.json(data.data);
// });
module.exports = router