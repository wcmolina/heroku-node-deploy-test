const express = require('express');
const router = express.Router();
const organizations = require('../data_migration/AllOrganizations')
const config = require('../db/config');
const utils = require('../utils');

const getBodyData = req => {
    return {
        ipfs_hash: req.body.ipfs_hash,
        blockchain_address: req.body.blockchain_address,
        require_photo: req.body.require_photo
    };
};

const changeStatus = async (statusName, ids) => {
    const status = await config.list('voters_status', {name: statusName});
    const status_id = !status.error && status.data.length > 0 ? status.data[0].id : null;
    if (status_id !== null) {
        const data = await config.db.manyOrNone(`UPDATE voters SET status_id = ${status_id} WHERE id IN (${ids.join(',')})`);
        return true;
    } else {
        return false;
    }
};
router.get('/', async (req, res) => {
    // res.json(organizations.data);
    const data = await config.list('organization');
    utils.setStatus(res, data.error);
    res.json(data.data);
})

router.get('/:id', async (req, res) => {
    const data = await config.list('organization', req.params.id);
    let retVal = {};
    if (!data.error && data.data.length > 0) {
        const voters = await config.list('voters', {organization_id: req.params.id});
        
        const authorities = await config.db.query(`SELECT * FROM organization INNER JOIN organization_users ON organization.id = organization_users.organization_ID INNER JOIN users ON users.id = organization_users.user_id INNER JOIN roles ON roles.id = users.role_id WHERE roles.name = 'Authority' AND organization.id = ${req.params.id};`);
        
        retVal = Object.assign(data.data[0], {voters: voters.error && voters.data.length === 0 ? [] : voters.data}, {authorities});

        
    }
    // const data = await config.db.query(`SELECT * from organization INNER JOIN voters ON organization.id = voters.organization_id WHERE organization.id = ${req.params.id};`);
    // utils.setStatus(res, data.error);
    res.json(retVal);
})

router.post('/add_authority', async (req, res) => {
    const data = await config.insert('organization_users', {
        organization_id: req.body.organization_id,
        user_id: req.body.user_id
    });
    utils.setStatus(res, data.error);
    res.json(data.data);
});
router.post('/', async (req, res) => {
    const data = await config.insert('organization', getBodyData(req));
    res.status(data.error ? 400 : 200);
    res.json(data.data);

})

router.post('/:id/voters', async (req, res) => {
    let data = await config.list('organization', req.params.id);
    res.status(data.error ? 400 : 200);
    if(!data.data){
    	res.status(404)
    	return
    }

    data = await config.insert('voters', {
        organization_id: req.params.id,
    	first_name:req.body.first_name,
    	second_name:req.body.second_name,
    	email:req.body.email,
        registration_id: req.body.registration_id,
        status_id:1,
        blockchain_address: req.body.blockchain_address,
    });
    res.status(data.error ? 400 : 200);
    res.json(data.data);    
})

router.get('/:id/voters', async (req, res) => {
    const data = await config.listJoin('voters','organization_id',req.params.id);
    res.status(data.error ? 400 : 200);
    res.json(data.data);    
})

router.post('/:id/authority', async (req, res) => {
    res.status(200).end()
})


router.put('/:id', async (req, res) => {
    const data = await config.update('organization', req.params.id, getBodyData(req));
    utils.setStatus(res, data.error);
    res.json(data.data);
})
router.delete("/:id", async (req, res) => {
    const data = await config.delete('organization', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});


router.post('/:id/approve', async (req, res) => {
    const completed = changeStatus('Approved', req.body.voters);
    res.status(completed ? 200 : 400);
    res.json({completed: Boolean(completed)});
});
router.post('/:id/reject', async (req, res) => {
    const completed = changeStatus('Rejected', req.body.voters);
    res.status(completed ? 200 : 400);
    res.json({completed: Boolean(completed)});
});
router.post('/:id/archive', async (req, res) => {
    const completed = changeStatus('Archived', req.body.voters);
    res.status(completed ? 200 : 400);
    res.json({completed: Boolean(completed)});
});
router.post('/:id/invite', async (req, res) => {
    const completed = changeStatus('Pending_Approval', req.body.voters);
    res.status(completed ? 200 : 400);
    res.json({completed: Boolean(completed)});
});
module.exports = router