const express = require('express');
const router = express.Router();
const multer = require('multer');
const csv = require('csv-parser')
const fs = require('fs')
const results = [];
const config = require('../db/config');
const utils = require('../utils');
const mailgun = require("mailgun-js");

const upload = multer({ dest: 'tmp/csv/' });

let importVoters = async function (voters){

	Promise.all(
	    voters.map(async (voter)=>{
		  await config.insert('voters',voter);
	    })
	)
}

router.post('/import',upload.single('file'),async function(req,res) {
  const results = [];

	fs.createReadStream(req.file.path)
	  .pipe(csv())
	  .on('data', (data) => results.push({
	  	first_name:data['firstName']||data['FirstName'],
	  	second_name:data['secondName']||data['SecondName'],
	  	email:data['email'],
	  	registration_id:data['id']||data['Id']
	  }))
	  .on('end', async () => {
	    fs.unlinkSync(req.file.path);
    	const data = await importVoters(results)
    	data.catch((e)=>{
    		res.status(400);
    	})
    	.then(()=>{
		    res.status(200);
    	})
	  });
})

router.post('/invite',upload.single('file'),async function(req,res) {
	const DOMAIN = "sandbox067f4089b2cd46b78b7566af45fae926.mailgun.org";
	const mg = mailgun({apiKey: "2f5be9c20cf6d66916732aedf30acf8d-7238b007-97c05542", domain: DOMAIN});
	const data = {
		from: "Balloted CMS Hackathong <ballotted@hackathong.org>",
		to: "didier@helloiconic.com",
		subject: "Hey, Join Ballotted!",
		text: "Run secure, transparent and fully auditable elections. Made Simple."
	};


	mg.messages().send(data, function (error, body) {
		utils.setStatus(res, error);
	});

})

module.exports = router