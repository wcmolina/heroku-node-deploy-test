const express = require('express');
const router = express.Router();
const users = require('../data_migration/AllUser')
const config = require('../db/config');
const utils = require('../utils');
const getBodyData = req => {
    return {
        user_name: req.body.user_name,
        complete_name: req.body.complete_name,
        blockchain_address: req.body.blockchain_address,
        email: req.body.email,
        role_id: req.body.role_id,
        password: req.body.password
    };
};

router.get('/', async (req, res) => {
    // const data = await config.list('users');
    // utils.setStatus(res, data.error)
    // res.json(data.data);
    const query = `SELECT users.*, roles.name AS role_name FROM users INNER JOIN roles ON users.role_id = roles.id;`;
    const data = await config.db.query(query);
    res.json(data);
    
})

router.get('/:id', async (req, res) => {
    // const data = await config.list('users', req.params.id);
    // utils.setStatus(res, data.error)
    // res.json(data.data);

    const query = `SELECT users.*, roles.name AS role_name FROM users INNER JOIN roles ON users.role_id = roles.id WHERE users.id = ${req.params.id}`;
    const data = await config.db.query(query);
    res.json(data);
});
router.post('/', async (req, res) => {

    if(req.session.role == "Authority"){
        const data = await config.insert('users', getBodyData(req));
        utils.setStatus(res, data.error);
        res.json(data.data);
    }else{
        res.status(403).end()
    }
});

router.delete("/:id", async (req, res) => {
    const data = await config.delete('users', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});
router.put('/:id', async (req, res) => {
    const data = await config.update('users', req.params.id, getBodyData(req));
    setStatus(res, data.error);
    res.json(data.data);
});

module.exports = router