const express = require('express');
const router = express.Router();
const config = require('../db/config');
const utils = require('../utils');
const awsUpload = require('../utils/awsUpload');
const multer = require('multer');
const fs = require('fs')
const upload = multer({ dest: 'tmp/imgs/' });

const getBodyData = req => {
    return {
        first_name: req.body.first_name,
        second_name: req.body.second_name,
        registration_id: req.body.registration_id,
        email: req.body.email,
        status_id: req.body.status_id,
        organization_id: req.body.organization_id,
        blockchain_address: req.body.blockchain_address,
        photo_selfie: req.body.photo_selfie,
        photo_id: req.body.photo_id
    };
};


router.get('/', async (req, res) => {
    const data = await config.list('voters');
    utils.setStatus(res, data.error);
    res.json(data.data);
});

router.get('/:id', async (req, res) => {
    const data = await config.list('voters', req.params.id);
    utils.setStatus(res, data.error);
    res.json(data.data);
});

router.put('/:id/photo_selfie', upload.single('file'), async (req, res) => {
    const data = await config.list('voters', req.params.id);
    
    if(data){
        awsUpload(req.file.path).then(async (response)=>{
            const voter_update = await config.update('voters', req.params.id,{
                photo_selfie:response.Location
            });
            utils.setStatus(res, voter_update.error);
            res.json(voter_update.data);            
        })
    }
    else{
        res.status(404)
    }
});

router.put('/:id/photo_id', upload.single('file'), async (req, res) => {
    const data = await config.list('voters', req.params.id);
    
    if(data){
        awsUpload(req.file.path).then(async (response)=>{
            const voter_update = await config.update('voters', req.params.id,{
                photo_id:response.Location
            });
            utils.setStatus(res, voter_update.error);
            res.json(voter_update.data);            
        })
    }
    else{
        res.status(404)
    }
});

router.post('/', async (req, res) => {
    const data = await config.insert('voters', getBodyData(req));
    utils.setStatus(res, data.error);
    res.json(data.data);
});
// const status = await config.list('voters_status', {name: 'Approved'});
// const status_id = !status.error && status.data.length > 0 ? status.data[0].id : null;
// if (status_id !== null) {
//     const data = await config.db.manyOrNone(`UPDATE voters SET status_id = ${status_id} WHERE id IN (${req.body.voters.join(',')})`);
//     res.json(data);
// } else {
//     res.json({})
// }
router.delete("/:id", async (req, res) => {
    const data = await config.delete('voters', req.params.id);
    utils.setStatus(res, data.error);
    res.json({
        error: data.error
    });
});
router.put('/:id', async (req, res) => {
    const data = await config.update('voters',req.params.id, getBodyData(req));
    utils.setStatus(res, data.error);
    res.json(data.data);
});
module.exports = router