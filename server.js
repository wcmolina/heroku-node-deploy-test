const express = require('express');
const cors = require('cors');
const session = require('express-session');
const bodyParser = require('body-parser');
const Knex = require('knex');
const { Model } = require('objection');
const path = require('path');
const knexConfig = require('./knexfile');

// API routes
const users = require('./routes/api/users');
const auth = require('./routes/api/auth');

const organizations = require('./routes/organizations');
const roles = require('./routes/roles');
const registration = require('./routes/registration');
const voters = require('./routes/voters');
const voterStatus = require('./routes/voters_status');
const organizationUsers = require('./routes/organization_users');
const elections = require('./routes/elections');
const electionStatus = require('./routes/election_status');

const app = express();

// Initialize Knex and give the Knex instance to Objection
const knex = Knex(knexConfig[app.get('env')]);
Model.knex(knex);

app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use(bodyParser.json());
app.use(cors({ origin: true, credentials: true }));

// Init Middleware
app.use(express.json({ extended: false }));

app.use(
    session({
        secret: 'secret',
        resave: true,
        saveUninitialized: true
    })
);

// app.get('/', (req, res) => res.send('Hello world!'));

// use Routes
app.use('/api/users', users);
app.use('/api/organizations', organizations);
app.use('/api/roles', roles);
app.use('/api/registration', registration);
app.use('/api/voters', voters);
app.use('/api/voters_status', voterStatus);
app.use('/api/organization_users', organizationUsers);
app.use('/api/elections', elections);
app.use('/api/election_status', electionStatus);
app.use('/api/auth', auth);

// Serve static assets in production
if (process.env.NODE_ENV === 'production') {
    // Set static folder
    app.use(express.static('client/build'));
    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
    });
}

const port = process.env.PORT || 8082;

app.get('/home', function(request, response) {
    if (request.session.loggedin) {
        response.send(`Welcome back, ${request.session.username}!`);
    } else {
        response.send('Please login to view this page!');
    }
    response.end();
});

app.listen(port, () => console.log(`Server running on port ${port}`));
