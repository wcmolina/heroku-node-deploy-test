module.exports = {
    setStatus: (res, error) => {
        res.status(error ? 400 : 200);
    }
};